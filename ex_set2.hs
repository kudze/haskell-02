module ExSet2 where
import Test.QuickCheck
import Data.Char
import Data.List

--ex 1
average :: [Float] -> Float
average [] = error "List is empty!"
average x = sum x / fromIntegral(length x)

--ex 2
dividesRecursive :: Integer -> [Integer]
dividesRecursive x = reverse (dividesRecursiveInner x x)
    where
        dividesRecursiveInner :: Integer -> Integer -> [Integer]
        dividesRecursiveInner _ 0 = []
        dividesRecursiveInner x current
            | mod x current == 0 = abs current : dividesRecursiveInner x (findNextIterator current)  
            | otherwise = dividesRecursiveInner x (findNextIterator current)
        findNextIterator :: Integer -> Integer
        findNextIterator 0 = error "This function should never be called with 0"
        findNextIterator x
            | x > 0 = x - 1
            | otherwise = x + 1

dividesListComprehension 0 = []
dividesListComprehension x
    | x > 0 = [n | n <- [1, 2 .. x], mod x n == 0]
    | otherwise = [abs n | n <- [-1, -2 .. x], mod x n == 0]

prop_divides :: Integer -> Bool
prop_divides x = dividesRecursive x == dividesListComprehension x

isPrime :: Integer -> Bool
isPrime x 
    | x > 0 = dividesListComprehension x == [1, x]
    | otherwise = error "Only defined for non-negative integers!"

--ex 3
prefix :: String -> String -> Bool
prefix [] _ = True
prefix _ [] = False
prefix (p:pt) (t:tt) = p == t && prefix pt tt

substring :: String -> String -> Bool
substring sub [] = False
substring sub (t:tt) = prefix sub (t:tt) || substring sub tt

--ex 4
permut :: [Integer] -> [Integer] -> Bool
permut a b = sort a == sort b

--ex 5
capitalise :: String -> String
capitalise text = [toUpper(t) | t<-text, not (isDigit t)]

--ex 6
itemTotal :: [(String, Float)] -> [(String, Float)]
itemTotal [] = []
itemTotal ((itemName, itemPrice):itemT) = 
    (itemName, itemPrice + sum [_itemPrice | (_itemName, _itemPrice) <- itemT, _itemName == itemName]) 
    : itemTotal [(__itemName, __itemPrice) | (__itemName, __itemPrice) <- itemT, __itemName /= itemName]

itemDiscount :: String -> Integer -> [(String, Float)] -> [(String, Float)]
itemDiscount key discount items
    | discount < 0 =  error "Discount may not be less than 0!"
    | discount > 100 = error "Discount may not be more than 100!"
    | otherwise = [if itemName == key then (itemName, itemPrice * (1 - (fromIntegral(discount)/100))) else (itemName, itemPrice) | (itemName, itemPrice) <- items]
